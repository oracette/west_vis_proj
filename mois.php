<?php
	require_once("action/MoisAjaxAction.php");

	$json = array();

	if(isset($_GET["annee"])){
		$action = new MoisAjaxAction($_GET["annee"]);
		$action->execute();

		$json = $action->mois;
	}

	echo json_encode($json);
