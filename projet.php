<?php
	require_once("action/ProjetAjaxAction.php");

	$json = array();

	if(isset($_GET["projet"])){
		$action = new ProjetAjaxAction($_GET["projet"]);
		$action->execute();

		$json = $action->donneesProjet;
	}

	echo json_encode($json);
