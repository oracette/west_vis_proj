<?php
	require_once("action/SuiviAjaxAction.php");

	$json = array();

	if(isset($_GET["projet"]) && isset($_GET["mois"])){
		$action = new SuiviAjaxAction($_GET["projet"], $_GET["mois"]);
		$action->execute();

		$json = $action->donneesSuivi;
	}

	echo json_encode($json);
