<?php
	require_once("action/AdminAction.php");

	$action = new AdminAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<script type="text/javascript" src="js/admin.js"></script>

	<?php
		if(!empty($action->erreur)){
	?>
		<h4 id="erreur" class="text-center"><?=$action->erreur?></h4>
	<?php
		}

		if(isset($_GET["erreur"])){
			//1 is true, true is 1
			if($_GET["erreur"] == true){
	?>
			<h4 id="erreur" class="text-center">Il y a eu une erreur avec votre requête. Réessayez s.v.p.</h4>
			<p class="text-center">Contactez un administrateur si l'erreur persiste.</p>
	<?php
			}
		}
	?>


	<?php
		if(isset($_SESSION["user"])){
	?>
			<div class="container">
				<div class="row panel panel-default">
					<div class="panel-body">
						<ul id="tab_menu" class="nav nav-tabs nav-justified">
						  <li class="active"><a data-toggle="tab" href="#ajouter_suivi">Ajouter un suivi</a></li>
						  <li><a data-toggle="tab" href="#ajouter_projet">Ajouter un projet</a></li>
						  <li><a data-toggle="tab" href="#changer_mdp">Changer de mot de passe</a></li>
						</ul>

						<!--Les trois formulaires-->
						<div class="tab-content">
							<!--Suivi-->
							<div id="ajouter_suivi" class="tab-pane fade in active">
								<div class="container-fluid">
									<form class="form" role="form" method="post" action="admin.php">
										<div class="row">
											<div class="col-xs-12 col-sm-4">
												<div class="form-group">
													<label class="sr-only" for="projet_select">Projet</label>
													<select id="projet_select" name="projet_select" class="form-control" required>
														<option value="" disabled selected>Projet</option>
														<?php
															if($action->projets){
																foreach($action->projets as $projet){
																	$id = $projet["id"];
																	$desc = $projet["desc"];

																	if($projet["retard"]){
																		$class="suivi_retard";
																	} else {
																		$class="suivi_ajour";
																	}
														?>
															<option value=<?=$id?> class="<?=$class?>"><?=$desc?></option>
														<?php
																}
															}
														?>
													</select>
										  		</div>
											</div>

											<div class="col-xs-12 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-5">
												<div class="form-group text-center">
													<div class="btn-group" data-toggle="buttons">
													  <label class="btn btn-danger">
													    <input type="radio" name="statut_radio" id="annule_radio" value="3" autocomplete="off"> Annulé
													  </label>
													  <label class="btn btn-info">
													    <input type="radio" name="statut_radio" id="complete_radio" value="4" autocomplete="off"> Complété
													  </label>
													</div>
											 	</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-3">
												<div class="form-group">
													<div class="input-group">
														<label class="sr-only" for="cout_revise">Coût révisé</label>
														<input min="0" id="cout_revise" type="number" class="form-control"name="cout_revise" placeholder="Coût révisé" required>
														<div class="input-group-addon">$</div>
													</div>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="debut_revise">Début revisé</label>
													<select class="form-control" name="debut_revise" id="debut_revise"required>
														<option value="" disabled selected>Début revisé</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="fin_revise">Fin revisée</label>
													<select class="form-control" name="fin_revise" id="fin_revise"required>
														<option value="" disabled selected>Fin revisée</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="conseil_revise">Conseil revisé</label>
													<select class="form-control" name="conseil_revise" id="conseil_revise">
														<option value="" disabled selected>Conseil revisé</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>

										<?php
										if($_SESSION["visibilite"] === CommunAction::$VIS_ADMIN){
										?>
										<div class="row small_border">
											<div class="col-md-3">
													<h4>En date du:</h4>
											</div>

											<div class="col-md-3">
												<div class="form-group">
													<label class="sr-only" for="annee_suivi">Année du suivi</label>
													<input min="2000" max="3000" type="number" id="annee_suivi" class="form-control"name="annee_suivi" placeholder="Année" required>
												</div>
											</div>

											<div class="col-md-3">
												<div class="form-group">
													<label class="sr-only" for="mois_suivi">Mois du suivi</label>
													<select class="form-control" name="mois_suivi" id="mois_suivi"required>
														<option value="" disabled selected>Mois</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<?php
										}
										?>

										<div class="row small_border">
											<div class="col-sm-2">
												<div class="form-group">
													<button class="btn btn-success" name="ajouter_suivi" type="submit" value="true">Ajouter</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

							<!--Projet-->
							<div id="ajouter_projet" class="tab-pane fade">
								<div class="container-fluid">
									<form class="form" role="form" method="post" action="admin.php">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group">
													<label class="sr-only" for="categorie_select">Catégorie</label>
													<select id="categorie_select" name="categorie_select" class="form-control" required>
														<option value="" disabled selected>Catégorie</option>
														<?php
															if($action->categories){
																foreach($action->categories as $categorie){
																	$id = $categorie["id"];
																	$cate = $categorie["categorie"];
														?>
															<option value=<?=$id?>><?=$cate?></option>
														<?php
																}
															}
														?>
													</select>
										  		</div>
											</div>

											<?php
												//Peux check pour soit les gestionnaires ou l'acces dans $_SESSION
												//Si ton acces est pas assez élevé, t'as pas de gestionnaires....
												if($action->gestionnaires){
											?>
												<div class="col-sm-4">
													<div class="form-group">
														<label class="sr-only" for="gestion_select">Gestionnaire</label>
														<select id="gestion_select" name="gestion_select" class="form-control" required>
															<option value="" disabled selected>Gestionnaire</option>
											<?php
													foreach($action->gestionnaires as $gestionnaire){
														$id = $gestionnaire["id"];
														$nom = $gestionnaire["nom"] . " " . $gestionnaire["prenom"];
											?>
															<option value=<?=$id?>><?=$nom?></option>
											<?php
													}
											?>
														</select>
													</div>
												</div>

												<div class="col-sm-4">
													<div class="form-group">
														<label class="sr-only" for="budget">Année</label>
														<input id="annee_projet" type="text" class="form-control"name="annee_projet" placeholder="Année">
													</div>
												</div>
											<?php
												}
											?>
										</div>

										<div class="row">
											<div class="col-sm-3">
												<div class="form-group">
													<div class="input-group">
														<label class="sr-only" for="budget">Budget</label>
															<input id="budget" type="text" class="form-control"name="budget" placeholder="Budget" required>
														<div class="input-group-addon">$</div>
													</div>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="debut_planif">Début planifié</label>
													<select class="form-control" name="debut_planif" id="debut_planif"required>
														<option value="" disabled selected>Début planifié</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="fin_planif">Fin planifiée</label>
													<select class="form-control" name="fin_planif" id="fin_planif"required>
														<option value="" disabled selected>Fin planifiée</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>

											<div class="col-sm-3">
												<div class="form-group">
													<label class="sr-only" for="conseil_planif">Conseil planifié</label>
													<select class="form-control" name="conseil_planif" id="conseil_planif"required>
														<option value="" disabled selected>Conseil planifié</option>
														<?php
															foreach($action->mois as $mois){
																$value=$mois["noMois"] . SPLIT_CHAR . $mois["mois"];
														?>
															<option value="<?=$value?>"><?=$value?></option>
														<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>

										<div class="row small_border">
											<div class="col-sm-12">
												<div class="form-group">
													<textarea name="description" class="form-control" rows="3" placeholder="Courte description du projet" required></textarea>
												</div>
											</div>
										</div>

										<div class="row small_border">
											<div class="col-sm-2">
												<div class="form-group">
													<button class="btn btn-success" name="ajouter_projet" type="submit" value="true">Ajouter</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

							<!--Mot de passe-->
							<div id="changer_mdp" class="tab-pane fade">
								<div class="container-fluid">
									<form class="form" role="form" method="post" action="admin.php">

										<?php
											//On veut les users, pas les gestionnaires
											if($action->usagers){
										?>
											<div class="col-sm-4 col-sm-offset-4">
												<div class="form-group">
													<label class="sr-only" for="usager_select">Usagers</label>
													<select id="usager_select" name="usager_select" class="form-control">
														<option value="" disabled selected>Usager</option>
										<?php
												foreach($action->usagers as $usager){
													$login = $usager["login"];

													//if($login != $_SESSION["user"]){

										?>
														<option value=<?=$login?>><?=$login?></option>
										<?php
													//}
												}
										?>
													</select>
												</div>
											</div>
										<?php
											}
										?>

										<div class="row">
											<div class="col-sm-12">
											  <div class="form-group">
												  <input type="password" class="form-control" id="mdp" placeholder="Nouveau mot de passe" name="mdp" required>
											  </div>
									  		</div>
										</div>

										<div class="row">
											<div class="col-sm-12">
											  <div class="form-group">
											      <input type="password" class="form-control" id="confirmation" placeholder="Confirmation" name="confirmation" required>
											  </div>
									  		</div>
										</div>

										<div class="row">
											<div class="col-sm-2">
											  	<div class="form-group">
												    <div>
												      <button class="btn btn-success" name="change_mdp" type="submit" value="true">Changer</button>
												    </div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div id="donnees_projet" class="container">
				<div class="row panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-4">
								<h3>Planification</h3>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3">
								<li class="list-group-item">Budget: <span id="budget_projet"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Début: <span id="debut_projet"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Fin: <span id="fin_projet"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Conseil: <span id="conseil_projet"></span></li>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--Dernier suivi entré-->
			<div id="suivi_precedent" class="container">
				<div class="row panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-4">
								<h3>Suivi précédent</h3>
							</div>
							<div class="col-sm-4 text-center">
								<p class="">Statut: <span id="statut_suivi"></span></p>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3">
								<li class="list-group-item">Coût: <span id="cout_suivi"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Début: <span id="debut_suivi"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Fin: <span id="fin_suivi"></span></li>
							</div>
							<div class="col-sm-3">
								<li class="list-group-item">Conseil: <span id="conseil_suivi"></span></li>
							</div>
						</div>
					</div>
				</div>
			</div>

	<?php
		} else{
	?>
		<div id="login" class="row">
			<form class="form-horizontal col-sm-6 col-sm-offset-3 panel panel-default" method="post" action="admin.php">
				<div class="panel-body">
					<div class="row">
					  <div class="form-group">
					      <input class="form-control" id="user" placeholder="Nom" name="user" required>
					  </div>
					</div>

					<div class="row">
					  <div class="form-group">
					      <input type="password" class="form-control" id="password" placeholder="Mot de passe" name="password" required>
					  </div>
					</div>

					<div class="row">
					  	<div class="form-group">
						    <div>
						      <button class="btn btn-success" name="login" type="submit">Login</button>
						    </div>
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php
	}
	?>
<?php
	require_once("partial/footer.php");
?>
