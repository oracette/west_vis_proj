<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>
			<link rel="stylesheet" type="text/css" href="external/datatables/css/jquery.dataTables.min.css">
			<script type="text/javascript" charset="utf8" src="external/datatables/js/jquery.dataTables.min.js"></script>

			<script type="text/javascript" src="external/progressbar/progressbar.min.js"></script>

			<link rel="stylesheet" type="text/css" href="css/index.css">
			<script type="text/javascript" src="js/index.js"></script>

			<div class="container-fluid">
				<div id="dropdowns" class="row">
					<div class="center-block panel panel-default">
						<form method="POST" action="index.php" class="form-group panel-body">
							<select id="annee_select" name="annee_select" class="form-control form_control_inline" required>
								<option value="" disabled selected>Année</option>
								<?php
									foreach($action->annees as $annee){
								?>
									<option value="<?=$annee?>"><?=$annee?></option>
								<?php
									}
								?>
							</select>


							<select id="mois_select" name="mois_select" class="form-control form_control_inline" disabled>
								<option value="" disabled selected>Mois</option>
							</select>

							<select id="gestion_select" name="gestion_select" class="form-control form_control_inline" disabled>
								<option value="" disabled selected>Gestionnaire</option>
								<?php
									$tous="0" . SPLIT_CHAR . "tous";
								?>
								<option value=<?=$tous?>>Tous</option>

								<?php
									foreach($action->gestionnaires as $gestionnaire){
										$nom = $gestionnaire["nom"] . " " . $gestionnaire["prenom"];
										$id = $gestionnaire["id"]  . SPLIT_CHAR . $nom;
								?>
									<option value="<?=$id?>"><?=$nom?></option>
								<?php
									}
								?>
							</select>
							<!--<input id="input" type="submit" value="Choisir" class="btn btn-success"/>-->
							<button type="submit" class="btn btn-success">Choisir</button>
						</form>
					</div>
				</div>
			</div>


			<?php
				if(isset($action->IDGestionChoisi) && isset($action->anneeChoisi)){
			?>
				<div class="row">
					<?php
						//center-block doesn't work here? col-centered doesn't work with the dropdown container? what is going on?
					?>
					<div id="selections" class="center-block">
						<div class="panel panel-default">
							<div class="panel-body">
								<h3>Pour <?=$action->gestionChoisi?>, <?=strtolower($action->moisChoisi)?> <?=$action->anneeChoisi?></h3>
								<p>Nombre de projets pour <?=$action->anneeChoisi?>: <span id="nbProjets" class="nombres_importants"> <?=$action->nbProjets?></span></p>
								<p>Ecart total du mois (budget - couts revisés): <span class="nombres_importants"><?=$action->formatNum($action->ecartTotal)?> $</span></p>
							</div>
						</div>
					</div>
				</div>

			<?php
			}
			if(isset($action->donneesDonut)){
			?>

				<div id="wrap_all_stats" class="container">
					<div class="row">
						<div class="col-md-12">
						<?php
							$nbDonuts = 0;
							foreach($action->donneesDonut as $donut){
								$colour = $donut["couleur"];
								$statID = "stat_" . $donut["id"];
								$stat = $donut["stat"];

								//"hard coded" pas mal, j'assume qu'il y aura toujours au moins une couche si action->donneesDonut n'est pas null.
								$couche1 = $donut["couches"]["couche_1"];
								$budget = $couche1["budget"];
								$revise = $couche1["revise"];
								$ecart = $couche1["ecart"];

								$paire = false;

								if($nbDonuts % 2 === 0){
									$paire = true;
								}

								//if($paire){

						?>
									<!--<div class="row">-->
						<?php
								//}
						?>
								<div class="stat_wrapper">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="donut_wrapper">
												<?php
													foreach($donut["couches"] as $couche){
														$compte = $couche["compte"];
														$layerID = $statID . "_" . $couche["id"];
														$textClass = "texte_" . $couche["id"];
												?>
													<div id=<?=$layerID?> class=<?=$statID?>>
														<p class=<?=$textClass?>><?=$compte?></p>
													</div>
												<?php
													}
												?>
											</div>

											<div class="stat_text_wrapper">
												<div class="">
													<h3><?=$stat?></h3>
												</div>

												<div class="">
													<table>
														<tr>
															<th>Budget:</th>
															<td class="currency_column"><?=$action->formatNum($budget)?> $</td>
														</tr>

														<tr>
															<th>Revisé:</th>
															<td class="currency_column"><?=$action->formatNum($revise)?> $</td>
														</tr>

														<tr>
															<th>Écart:</th>
															<td class="currency_column"><?=$action->formatNum($ecart)?> $</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
						<?php
								//if(!$paire){
						?>
									<!--</div>-->
						<?php
								//}
								$nbDonuts++;
							}
						?>
						</div>
					</div>
				</div>
				<?php
				}
				?>

		<div class="row">
			<?php
				if(isset($action->donneesTable)){
			?>
				<table id="table_suivis" class="cell-border compact stripe hover">
					<thead>
						<tr>
							<th></th>
							<th>Categorie</th>
							<th>Description</th>
							<th>Révisions</th>
							<th>Statut</th>
							<?php
								foreach($action->mois as $mois){
									if($mois["mois"] === "Juin"){
										$nomMois = "Jun";
									} else {
										$nomMois = mb_substr($mois["mois"], 0, 3, 'utf8');
									}
							?>
									<th><?=$nomMois?></th>
							<?php
								}
							?>

						</tr>
					</thead>

					<tbody>
						<?php
							foreach($action->donneesTable as $row){
								$cate = $row["categorie"];
								$desc = $row["description"];
								$estime = $row["estime"];
								$couleurStat = $row["couleur"];
								$debut = $row["debut"];
								$fin = $row["fin"];
								$conseil = $row["conseil"];
								$idStat = $row["idStat"];
						?>
							<tr>
								<td></td>
								<td><?=$cate?></td>
								<td><?=$desc?></td>
								<td class="currency_column"><?=$action->formatNum($estime)?> $</td>
								<td data-order="<?=$idStat?>" bgcolor="<?=$couleurStat?>"></td>

								<?php
									foreach($action->mois as $mois){
										$noMois = $mois["noMois"];
										$couleur = "";

										if($noMois === $conseil){
											$couleur="#e05252";
										} else if($noMois >= $debut && $noMois <= $fin){
											$couleur="#f0e4c1";
										}



								?>
									<td bgcolor="<?=$couleur?>"></td>
								<?php
									}
								?>
							</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			<?php
				}
			?>
		</div>
<?php
	require_once("partial/footer.php");
?>
