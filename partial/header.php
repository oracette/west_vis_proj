<?php
  //So we can see errors
  //Remove these lines when you are done!!
  ini_set('display_startup_errors', 1);
  ini_set('display_errors', 1);
  error_reporting(-1);


  function customError($errno, $errstr) {
    echo "<b>Error:</b> [$errno] $errstr";
  }

  set_error_handler("customError");
?>

<!DOCTYPE html>
<html>
	<!--Par  Olivier Racette-->
	<head>
		<meta charset="UTF-8">

		<!--jQuery 2.2.4-->
		<script   src="external/jquery/jquery.min.js"></script>

		<!--Bootstrap CSS,JS 3.3.6-->
		<link rel="stylesheet" href="external/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="external/bootstrap/css/bootstrap-theme.min.css">
		<script src="external/bootstrap/js/bootstrap.min.js"></script>

		<!--CSS Global-->
		<link rel="stylesheet" type="text/css" href="css/global.css">

		<!--JS Global-->
		<script type="text/javascript" src="js/global.js"></script>

		<!--Tab icon-->
		<link rel="icon" type="image/png" href="favicon.png" sizes="16x16" />
	</head>

	<body>
		<div id="header_wrapper" class="row panel panel-default">
			<header class="col-md-6 col-md-offset-3 panel-body">
				<a href="http://www.westmount.org"><img src="img/logo-westmount.jpg" alt="Westmount logo" height="73" width="250"></a>
				<h2>Bureau de projets</h2>
			</header>
		</div>
