<?php
	require_once("action/CompareComptesAjaxAction.php");

	$json = array();

	if(isset($_GET["date"])){
		$action = new CompareComptesAjaxAction($_GET["date"]);
		$action->execute();

		$json = $action->comptes;
	}

	echo json_encode($json);
