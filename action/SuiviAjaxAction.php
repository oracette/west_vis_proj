<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/SuivisDAO.php");

	class SuiviAjaxAction extends CommunAction {
		public $donneesSuivi;

		public function __construct($projet, $mois){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);
			$this->donneesSuivi = SuivisDAO::getSuiviParProjetMoisAnnee($projet, $mois, date("Y"));
		}

		protected function executeAction(){

		}
	}
