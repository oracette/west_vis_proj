<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/ProjetsDAO.php");
	require_once("action/DAO/SuivisDAO.php");

	class ProjetAjaxAction extends CommunAction {
		public $donneesProjet;

		public function __construct($projet){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);
			$this->donneesProjet = ProjetsDAO::getPlanifProjetParId($projet);
		}

		protected function executeAction(){

		}
	}
