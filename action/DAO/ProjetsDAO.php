<?php
	class ProjetsDAO{
		public static function getNbProjetsParAnneeGestion($annee, $gestion){
			$query="SELECT COUNT(*) FROM tblProjets WHERE YEAR(Debut) = ?";

			if($gestion > 0){
				$query = $query . " AND ID_Gestionnaire = ?";
			}

			$conn = Connection::getConnection();
			$result = 0;

			if($stmt= $conn->prepare($query)){
				if($gestion > 0){
					$stmt->bind_param('si', $annee, $gestion);
				}
				else{
					$stmt->bind_param('s', $annee);
				}

				$stmt->execute();
				$stmt->bind_result($count);

				while($stmt->fetch()){
					$result = $count;
				}
				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function getProjetsParAnnee($annee){
			$conn = Connection::getConnection();
			$result = array();
			$query = "SELECT ID_Projet, Description FROM tblProjets WHERE YEAR(Debut) = ? ORDER BY Description";

			if($stmt= $conn->prepare($query)){
				$stmt->bind_param('s', $annee);
				$stmt->execute();
				$stmt->bind_result($id, $desc);

				while($stmt->fetch()){
					$row = array();
					$row["id"] = $id;
					$row["desc"] = $desc;
					array_push($result, $row);
				}
				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function getProjetsParLoginAnnee($login, $annee){
			$conn = Connection::getConnection();
			$query = "SELECT ID_Projet, Description FROM tblProjets JOIN tblUsagers ON tblProjets.ID_Gestionnaire = tblUsagers.ID_Gestionnaire WHERE Login = ? AND YEAR(Debut) = ? ORDER BY Description";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('ss', $login, $annee);
				$stmt->execute();
				$stmt->bind_result($id, $desc);

				while($stmt->fetch()){
					$row = array();
					$row["id"] = $id;
					$row["desc"] = $desc;
					array_push($result, $row);
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function ajouterProjet($categorie, $gestion, $budget, $conseil, $debut, $fin, $desc){
			$conn = Connection::getConnection();
			$query = "INSERT INTO tblProjets (ID_Categorie, ID_Gestionnaire, Budget, Conseil, Debut, Fin, Description) VALUES (?,?,?,?,?,?,?)";
			$erreur = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('iidssss', $categorie, $gestion, $budget, $conseil, $debut, $fin, $desc);
				$stmt->execute();
				$stmt->close();
			}

			if(mysqli_error($conn)){
				$erreur = true;
			}

			Connection::closeConnection();

			return $erreur;
		}

		public static function getPlanifProjetParId($id){
			$conn = Connection::getConnection();
			$query = "SELECT Budget, MONTHNAME(Conseil), MONTHNAME(Debut), MONTHNAME(Fin) FROM tblProjets WHERE ID_Projet = ?";
			$result = array();

			Connection::setLocaleFRCA();

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('i', $id);
				$stmt->execute();
				$stmt->bind_result($budget, $conseil, $debut, $fin);

				while($stmt->fetch()){
					$aucun = "Aucune date planifiée";;

					//Assuming only one is returned...
					if(!isset($conseil)){
						$conseil = $aucun;
					}

					if(!isset($debut)){
						$debut = $aucun;
					}

					if(!isset($fin)){
						$fin = $aucun;
					}

					$result["budget"] = CommunAction::formatNum($budget);
					$result["conseil"] = ucfirst($conseil);
					$result["debut"] = ucfirst($debut);
					$result["fin"] = ucfirst($fin);
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function compareComptes($date){
			//requete pour comparer les comptes de projets totaux de l'année avec le compte de suivis de projets d'un mois
			//retourne ID de gestionnaire si les comptes sont différents
			//Select id des comptes d'une table comporté d'id et comptes de tblProjets, groupé par ids jummelé à une autre table
			//	qui contient id et comptes de tblSuivis pour le mois et années courants
			// 	Assume 0 si un id provenant de tblSuivis est null (pas de compte)

			$conn = Connection::getConnection();
			$query = "SELECT c1.ID_Gestionnaire
						FROM (SELECT COUNT(ID_Projet) as count, ID_Gestionnaire
								FROM tblProjets WHERE YEAR(Debut) = YEAR(?)
								GROUP BY ID_Gestionnaire
								ORDER BY ID_Gestionnaire ) c1
						LEFT JOIN (SELECT COUNT(ID_Suivi) as count, tblProjets.ID_Gestionnaire
									FROM tblSuivis
									JOIN tblProjets
									ON tblSuivis.ID_Projet = tblProjets.ID_Projet
									WHERE MONTH(Date) = MONTH(?) AND YEAR(Date) = YEAR(?)
									GROUP BY tblProjets.ID_Gestionnaire
									ORDER BY tblProjets.ID_Gestionnaire) c2
						ON c1.ID_Gestionnaire = c2.ID_Gestionnaire
						WHERE c1.count <> IFNULL(c2.count,0)";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('sss', $date, $date, $date);
				$stmt->execute();
				$stmt->bind_result($id);

				while($stmt->fetch()){
					array_push($result, $id);
				}

				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}

		public static function getFinPlanif($projet){
			$conn = Connection::getConnection();
			$result = 0;
			$query = "SELECT MONTH(Fin) FROM tblProjets WHERE ID_Projet = ?";

			if($stmt= $conn->prepare($query)){
				$stmt->bind_param('i', $projet);
				$stmt->execute();
				$stmt->bind_result($fin);

				while($stmt->fetch()){
					$result = $fin;
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}
	}
