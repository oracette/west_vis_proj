<?php
	class CategoriesDAO{
		public static function getCategories(){
			$conn = Connection::getConnection();
			$query = "SELECT ID_Categorie, Categorie FROM tblCategories";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->execute();
				$stmt->bind_result($id, $cate);

				while($stmt->fetch()){
					$row = array();
					$row["id"] = $id;
					$row["categorie"] = $cate;

					array_push($result, $row);
				}
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}
	}
