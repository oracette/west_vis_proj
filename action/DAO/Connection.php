<?php
	class Connection{
		private static $connection = null;

		public static function getConnection(){
			if(Connection::$connection == null){
				Connection::$connection =  new mysqli(DB_LOCATION, DB_USER, DB_PASS, DB_ALIAS);
				mysqli_set_charset(Connection::$connection, DB_CHARSET);
			}

			return Connection::$connection;
		}

		public static function closeConnection(){
			mysqli_close(Connection::$connection);
			Connection::$connection=null;
		}

		public static function setLocaleFRCA(){
			/*Pour avoir les noms de mois en francais svp!*/

			$query = "SET lc_time_names = 'fr_CA';";
			if($stmt = Connection::$connection->prepare($query)){
				$stmt->execute();
			}
		}
	}
