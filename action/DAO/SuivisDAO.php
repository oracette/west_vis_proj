<?php
	class SuivisDAO{
		public static function getAnnees(){
			$conn = Connection::getConnection();
			$query = "SELECT DISTINCT YEAR(Date) FROM tblSuivis";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->execute();
				$stmt->bind_result($annee);

				while($stmt->fetch()){
					array_push($result, $annee);
				}
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}

		public static function getDonneesDonut($annee, $mois, $statut, $gestion){
			//Get values for revise instead
			$query = "SELECT SUM(Budget), SUM(CoutRevise), COUNT(*) FROM tblSuivis JOIN tblProjets ON tblSuivis.ID_Projet = tblProjets.ID_Projet WHERE YEAR(Date) = ? AND MONTH(Date) = ? AND ID_Statut = ?";

			if($gestion > 0){
				$query = $query . " AND ID_Gestionnaire = ?";
			}

			$result = array();

			$conn = Connection::getConnection();

			if($stmt = $conn->prepare($query)){
				if($gestion > 0){
					$stmt->bind_param('ssii', $annee, $mois, $statut, $gestion);
				}else{
					$stmt->bind_param('ssi', $annee, $mois, $statut);
				}

				$stmt->execute();
				$stmt->bind_result($budget, $revise, $compte);

				while($stmt->fetch()){
					if(empty($budget)){
						$budget = 0;
					}

					if(empty($estime)){
						$estime = 0;
					}

					$result["budget"] = $budget;
					$result["revise"] = $revise;
					$result["ecart"] = $budget - $revise;
					$result["compte"] = $compte;
				}
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}

		public static function getDonneesTable($annee, $mois, $gestion){
			//Need to get values for revise instead
			$query = "SELECT Categorie, Description, CoutRevise, Couleur, MONTH(DebutRevise), MONTH(FinRevise), MONTH(ConseilRevise), tblSuivis.ID_Statut FROM tblSuivis JOIN tblProjets ON tblSuivis.ID_Projet = tblProjets.ID_Projet JOIN tblCategories ON tblProjets.ID_Categorie = tblCategories.ID_Categorie JOIN tblStatuts ON tblSuivis.ID_Statut = tblStatuts.ID_Statut WHERE YEAR(Date) = ? AND MONTH(Date) = ?";

			if($gestion > 0){
				$query = $query . " AND ID_Gestionnaire = ?";
			}

			$result = array();

			$conn = Connection::getConnection();

			if($stmt = $conn->prepare($query)){
				if($gestion > 0){
					$stmt->bind_param('ssi', $annee, $mois, $gestion);
				} else {
					$stmt->bind_param('ss', $annee, $mois);
				}

				$stmt->execute();
				$stmt->bind_result($cate, $desc, $estime, $couleur, $debut, $fin, $conseil, $idStat);

				while($stmt->fetch()){
					$row = array();
					$row["categorie"] = $cate;
					$row["description"] = $desc;
					$row["estime"] = $estime;
					$row["couleur"] = $couleur;
					$row["debut"] = $debut;
					$row["fin"] = $fin;
					$row["conseil"] = $conseil;
					$row["idStat"] = $idStat;
					array_push($result, $row);
				}
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}

		public static function ajouterSuivi($projet, $statut, $cout, $debut, $fin, $conseil, $entree){
			$conn = Connection::getConnection();
			$query = "INSERT INTO tblSuivis (ID_Projet, ID_Statut, Date, CoutRevise, DebutRevise, FinRevise, ConseilRevise) VALUES (?,?,?,?,?,?,?)";
			$erreur = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('iisdsss', $projet, $statut, $entree, $cout, $debut, $fin, $conseil);
				$stmt->execute();

				if(mysqli_error($conn)){
					//Maybe it already exists, go update instead
					$erreur = SuivisDAO::updateSuivi($conn, $projet, $statut, $entree, $cout, $debut, $fin, $conseil);
				}
				$stmt->close();
			}

			Connection::closeConnection();

			return $erreur;
		}

		public static function updateSuivi($conn, $projet, $statut, $entree, $cout, $debut, $fin, $conseil){
			$query = "UPDATE tblSuivis SET ID_Statut=?,CoutRevise=?, DebutRevise=?, FinRevise=?, ConseilRevise = ? WHERE ID_Projet = ? AND Date = ?";
			$erreur = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('idsssis', $statut, $cout, $debut, $fin, $conseil, $projet, $entree);
				$stmt->execute();
				$stmt->close();
			}

			if(mysqli_error($conn)){
				$erreur = true;
			}

			return $erreur;
		}

		public static function GetEcartTotalParAnneeMoisGestion($annee, $mois, $gestion){
			$query = "SELECT SUM(Budget) - SUM(CoutRevise) FROM tblSuivis JOIN tblProjets ON tblSuivis.ID_Projet = tblProjets.ID_Projet WHERE Year(Debut)=? AND MONTH(Date)=?";

			if($gestion > 0){
				$query = $query . " AND ID_Gestionnaire = ?";
			}

			$conn = Connection::getConnection();
			$reslut = 0.00;

			if($stmt = $conn->prepare($query)){
				if($gestion > 0){
					$stmt->bind_param('ssi', $annee, $mois, $gestion);
				}
				else{
					$stmt->bind_param('ss', $annee, $mois);
				}
				$stmt->execute();
				$stmt->bind_result($total);

				while($stmt->fetch()){
					$result = $total;
				}
				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function getSuiviParProjetMoisAnnee($projet, $mois, $annee){
			$conn = Connection::getConnection();
			$query = "SELECT Statut, CoutRevise, MONTHNAME(DebutRevise), MONTHNAME(FinRevise), MONTHNAME(ConseilRevise), Couleur FROM tblSuivis JOIN tblStatuts ON tblSuivis.ID_Statut = tblStatuts.ID_Statut WHERE ID_Projet = ? AND MONTH(Date) = ? AND YEAR(Date) = ?";
			$result = array();

			Connection::setLocaleFRCA();

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('iss', $projet, $mois, $annee);

				$stmt->execute();
				$stmt->bind_result($statut, $cout, $debut, $fin, $conseil, $couleur);

				while($stmt->fetch()){
					$result["statut"] = $statut;
					$result["cout"] = CommunAction::formatNum($cout);
					$result["debut"] = ucfirst($debut);
					$result["fin"] = ucfirst($fin);
					$result["couleur"] = $couleur;

					if(empty($conseil)){
						$result["conseil"] = "Aucun";
					} else {
						$result["conseil"] = ucfirst($conseil);
					}
				}
				$stmt->close();
			}

			if(empty($result)){
				$result["statut"] = "Aucunes données pour le mois précédent.";
				$result["cout"] = 0;
				$result["debut"] = "";
				$result["fin"] = "";
				$result["conseil"] = "";
				$result["couleur"] = "#ffa64d";
			}

			Connection::closeConnection();
			return $result;
		}

		public static function getMoisParAnnee($annee){
			$conn = Connection::getConnection();
			$query = "SELECT DISTINCT MONTH(Date), MONTHNAME(Date) FROM tblSuivis WHERE Year(Date) = ? ORDER BY MONTH(Date)";
			$result = array();

			Connection::setLocaleFRCA();

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('s', $annee);

				$stmt->execute();
				$stmt->bind_result($mois, $nomMois);

				while($stmt->fetch()){
					$row = array();

					$nomMois = ucfirst($nomMois);
					$val = $mois . SPLIT_CHAR . $nomMois;

					array_push($result, $val);
				}
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}

		public static function checkRetard($projet, $annee, $mois){
			$conn = Connection::getConnection();
			$query = "SELECT ID_Suivi FROM tblSuivis WHERE ID_Projet = ? AND YEAR(Date) = ? AND MONTH(Date) = ?";
			$retard = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param("iss", $projet, $annee, $mois);

				$stmt->execute();
				$stmt->bind_result($suivi);

				//num_rows is bugged, always returns 0. nice.
				if(!$stmt->fetch()){
					$retard = true;
				}

				$stmt->close();
			}

			Connection::closeConnection();
			return $retard;
		}
	}
