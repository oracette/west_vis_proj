<?php
	class UsagersDAO{
		public static function login($user, $pw){
			$conn = Connection::getConnection();
			$query = "SELECT Mdp FROM tblUsagers WHERE Login = ?";
			$result = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('s', $user);
				$stmt->execute();
				$stmt->bind_result($hash);


				while($stmt->fetch()){
					//assuming we only get a single password from the query...¯\_(ツ)_/¯...login is unique anyway
					if(password_verify($pw, $hash)){
						$result = true;
					}
				}
				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function getAcces($login){
			$conn = Connection::getConnection();
			$query = "SELECT Acces FROM tblUsagers WHERE Login = ?";
			$result = 0;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('s', $login);
				$stmt->execute();
				$stmt->bind_result($acces);

				while($stmt->fetch()){
					$result = $acces;
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function changeMdp($login, $mdp){
			$conn = Connection::getConnection();
			$query = "UPDATE tblUsagers SET Mdp = ? WHERE Login = ?";
			$erreur = false;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('ss', $mdp, $login);
				$stmt->execute();
				$stmt->close();
			}

			if(mysqli_error($conn)){
				$erreur = true;
			}

			Connection::closeConnection();

			return $erreur;
		}

		public static function getLogins(){
			$conn = Connection::getConnection();
			$query = "SELECT Login FROM tblUsagers";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->execute();
				$stmt->bind_result($login);

				while($stmt->fetch()){
					$row = array();
					$row["login"] = $login;
					array_push($result, $row);
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}
	}
