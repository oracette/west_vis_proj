<?php
	class GestionnairesDAO{
		public static function getGestionnaires(){
			$conn = Connection::getConnection();
			$query = "SELECT ID_Gestionnaire, Nom, Prenom FROM tblGestionnaires";
			$result = array();


			if($stmt = $conn->prepare($query)){
				$stmt->execute();
				$stmt->bind_result($id, $nom, $prenom);

				while($stmt->fetch()){
					$row = array();
					$row["id"]=$id;
					$row["nom"] = $nom;
					$row["prenom"] = $prenom;

					array_push($result, $row);
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}

		public static function getGestionID($login){
			$conn = Connection::getConnection();
			$query = "SELECT tblGestionnaires.ID_Gestionnaire FROM tblGestionnaires JOIN tblUsagers ON tblGestionnaires.ID_Gestionnaire = tblUsagers.ID_Gestionnaire WHERE Login = ?";
			$result = NULL;

			if($stmt = $conn->prepare($query)){
				$stmt->bind_param('s', $login);
				$stmt->execute();
				$stmt->bind_result($id);

				while($stmt->fetch()){
					$result = $id;
				}

				$stmt->close();
			}

			Connection::closeConnection();

			return $result;
		}
	}
