<?php
	class StatutsDAO{
		public static function getStatuts(){
			$conn = Connection::getConnection();
			$query = "SELECT ID_Statut, Statut, Couleur FROM tblStatuts";
			$result = array();

			if($stmt = $conn->prepare($query)){
				$stmt->execute();
				$stmt->bind_result($id, $statut, $couleur);

				while($stmt->fetch()){
					$row = array();
					$row["id"] = $id;
					$row["stat"] = $statut;
					$row["couleur"] = $couleur;
					array_push($result, $row);
				}
				
				$stmt->close();
			}

			Connection::closeConnection();
			return $result;
		}
	}
