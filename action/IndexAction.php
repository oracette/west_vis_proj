<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/SuivisDAO.php");
	require_once("action/DAO/ProjetsDAO.php");
	require_once("action/DAO/GestionnairesDAO.php");
	require_once("action/DAO/StatutsDAO.php");
	require_once("action/DAO/CategoriesDAO.php");

	class IndexAction extends CommunAction {
		public $gestionnaires;
		public $annees;
		public $mois;
		public $gestionChoisi;
		public $IDGestionChoisi;
		public $anneeChoisi;
		public $moisChoisi;
		public $IDMoisChoisi;
		public $donneesDonut;
		public $donneesTable;
		public $nbProjets;
		public $ecartTotal;
		public $statuts;
		public $nbCouchesDonut;
		public $json;

		public function __construct(){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);

			$this->nbProjets = 0;
			$this->ecarTotal = 0;
			$this->nbCouchesDonut = 2;
			$this->json = 'donneesDonut.json';
		}

		protected function executeAction(){
			//Comportement de la page

			/*
				Pour l'instant:
					Page publique
					Données requises:
						Les années, mois et gestionnaires de projets (inputs)
						Pour les donuts:
							Nombre de projets, budgets, estimés, écarts ordonés par statuts, filtrés par année, moi, gestionnaire
						Pour la table:
							Projets filtrés par année, moi, gestionnaire
			*/
			if(isset($_POST["annee_select"])){
				$this->anneeChoisi=$_POST["annee_select"];
			}

			if(isset($_POST["mois_select"])){
				$this->IDMoisChoisi = $this->getSelectMoisNum($_POST["mois_select"]);
				$this->moisChoisi = $this->getSelectMoisNom($_POST["mois_select"]);
			}

			if(isset($_POST["gestion_select"])){
				$this->IDGestionChoisi = explode(SPLIT_CHAR, $_POST["gestion_select"])[0];
				$this->gestionChoisi = explode(SPLIT_CHAR, $_POST["gestion_select"])[1];
			}

			//remplir les dropdowns
			$this->gestionnaires=GestionnairesDAO::getGestionnaires();
			$this->annees=SuivisDAO::getAnnees();
			$this->mois = $this->remplirSelectMois();
			$this->statuts = StatutsDAO::GetStatuts();

			//Valeurs par défaut pour débug
			/*$this->anneeChoisi="2016";
			$this->moisChoisi="Janvier";
			$this->IDMoisChoisi=1;
			$this->IDGestionChoisi=0;
			$this->gestionChoisi = "test";*/

			//Si on a tous les inputs on peut finalement procéder
			if(isset($this->anneeChoisi) && isset($this->IDGestionChoisi) && isset($this->IDMoisChoisi)){
				$annee = $this->anneeChoisi;
				$numMois = $this->IDMoisChoisi;
				$idGestion = $this->IDGestionChoisi;

				$this->nbProjets = ProjetsDAO::getNbProjetsParAnneeGestion($annee, $idGestion);
				$this->ecartTotal = SuivisDAO::GetEcartTotalParAnneeMoisGestion($annee, $numMois, $idGestion);

				$this->donneesDonut = $this->creerTblDonnesDonut($annee, $numMois, $idGestion, $this->nbCouchesDonut);
				file_put_contents($this->json, json_encode($this->donneesDonut));

				$this->donneesTable = $this->creerTblDonnesTable($annee, $numMois, $idGestion);
			}
		}

		public function creerTblDonnesDonut($annee, $numMois, $idGestion, $nbCouches){
			//Créer tableau de données du donut

			//4 graph style donut avec 2 couches chacuns
			//chaque représente un statut
			//Premiere couche = le mois choisi
			//Deuxieme couche = le mois qui précéde le mois choisi

			$result = array();

			foreach($this->statuts as $statut){
				$donut = array_merge($statut, array());
				$couches = array();

				for($i = 0; $i < $nbCouches; $i++){
					$key = "couche_" . (string)($i + 1);

					$mois = $numMois - $i;
					$ann = $annee;

					if($mois === 0){
						$mois = 12;
						$ann = (string)(((int)$annee) - 1); //maybe a bit overdone here, just making sure string - int will be OK. You never know with PHP...
					}

					$couches[$key] = SuivisDAO::getDonneesDonut($ann, $mois, $statut["id"], $idGestion);
					$couches[$key]["id"] = $key;
				}
				$donut["couches"] = $couches;
				array_push($result,$donut);
			}
			return $result;
		}

		public function creerTblDonnesTable($annee, $numMois, $idGestion){
			//Créer tableau de données pour la table
			//Contiens: catégorie, projet, estimé, statut (in color), statut ID, debut, fin, conseil
			$data = SuivisDAO::getDonneesTable($annee, $numMois, $idGestion);

			return $data;
		}
	}
