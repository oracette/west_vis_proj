<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/UsagersDAO.php");
	require_once("action/DAO/ProjetsDAO.php");
	require_once("action/DAO/StatutsDAO.php");
	require_once("action/DAO/CategoriesDAO.php");
	require_once("action/DAO/GestionnairesDAO.php");
	require_once("action/DAO/SuivisDAO.php");

	class AdminAction extends CommunAction {
		public $projets;
		public $statuts;
		public $categories;
		public $gestionnaires;
		public $usagers;
		public $mois;

		public function __construct(){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);
		}

		protected function executeAction(){
			if(isset($_POST["user"]) && isset($_POST["password"])){
				$user = $_POST["user"];
				$pw = $_POST["password"];

				if(UsagersDAO::login($user, $pw)){
					$_SESSION["visibilite"] = UsagersDAO::getAcces($user);
					$_SESSION["user"] = $user;

					if($_SESSION["visibilite"] === CommunAction::$VIS_MOD){
						$_SESSION["gestion_id"] = GestionnairesDAO::getGestionID($user);
					}

					header('Location:'. $_SERVER['PHP_SELF']);
					exit();
				} else {
					$this->erreur = "Nom d'utilisateur ou mot de passe invalide";
				}
			} else if($this->isLoggedIn()){
				//If user is logged in we need to fetch other info
				$this->getSelectionInfo($_SESSION["user"], $_SESSION["visibilite"]);

				$erreur = false;

				if(isset($_POST["ajouter_suivi"])){
					$erreur = $this->ajouterSuivi();
				} else if(isset($_POST["ajouter_projet"])){
					$erreur = $this->ajouterProjet();
				} else if(isset($_POST["change_mdp"])){
					$erreur = $this->changeMdp();
				}

				//Peu importe ce qui a été sousmis on fait la même chose.
				//Faut passer l'erreur en GET puisque la page est refraichie.
				if(isset($_POST["ajouter_suivi"]) || isset($_POST["ajouter_projet"]) || isset($_POST["change_mdp"])){
					$_POST = array();
					header('Location:'. $_SERVER['PHP_SELF'] . '?erreur=' . $erreur);
					exit;
				}
			}
		}

		public function getSelectionInfo($login, $acces){
			//populate dropdowns for forms
			//Needed into:
			//	projects of current user
			//	statuts
			//	catégories
			//	gestionnaires if admin
			//mois pour: début, fin révisé
			//			début, fin

			$this->projets = $this->getProjets($login, $acces);
			$this->gestionnaires = $this->getGestionnaires($acces);
			$this->usagers = $this->getUsagers($acces);

			$this->statuts = StatutsDAO::getStatuts();
			$this->categories = CategoriesDAO::getCategories();
			$this->mois = $this->remplirSelectMois();
		}

		public function getUsagers($acces){
			if($acces === CommunAction::$VIS_ADMIN){
				$result = UsagersDAO::getLogins();
			} else{
				$result = NULL;
			}

			return $result;
		}

		public function getGestionnaires($acces){
			if($acces === CommunAction::$VIS_ADMIN){
				$result = GestionnairesDAO::getGestionnaires();
			} else{
				$result = NULL;
			}

			return $result;
		}

		public function getProjets($login, $acces){
			$annee = date("Y");
			$mois = date("m");

			if($acces === CommunAction::$VIS_ADMIN){
				$result = ProjetsDAO::getProjetsParAnnee($annee);
			} else{
				$result = ProjetsDAO::getProjetsParLoginAnnee($login, $annee);
			}

			//result is now an array of projets. Check if each project has been updated this month already
			for($i = 0; $i < sizeof($result); $i++){
				$result[$i]["retard"] = SuivisDAO::checkRetard($result[$i]["id"], $annee, $mois);
			}

			return $result;
		}

		public function ajouterSuivi(){
			$erreur = false;

			if(isset($_POST["projet_select"]) && isset($_POST["cout_revise"]) && isset($_POST["debut_revise"]) && isset($_POST["fin_revise"])){
				$annee = date("Y");

				$dateEntree = 0;

				if(isset($_POST["annee_suivi"]) && isset($_POST["mois_suivi"])){
					$dateEntree = $this->creerDate($_POST["annee_suivi"], $this->getSelectMoisNum($_POST["mois_suivi"]));
				} else {
					$timestamp = getdate();
					$dateEntree = $timestamp["year"] . "-" . $timestamp["mon"] . "-00";
				}

				if($dateEntree != 0){
					$idProjet = $_POST["projet_select"];
					$coutRevise = $_POST["cout_revise"];
					$debutRevise = $this->creerDate($annee, $this->getSelectMoisNum($_POST["debut_revise"]));
					$finRevise = $this->creerDate($annee, $this->getSelectMoisNum($_POST["fin_revise"]));

					$idStatut = $this->reviseStatut($this->getSelectMoisNum($_POST["fin_revise"]), $idProjet);

					if(isset($_POST["conseil_revise"])){
						$conseilRevise = $this->creerDate($annee, $this->getSelectMoisNum($_POST["conseil_revise"]));
					} else {
						$conseilRevise = null;
					}

					$erreur = SuivisDAO::ajouterSuivi($idProjet, $idStatut, $coutRevise, $debutRevise, $finRevise, $conseilRevise, $dateEntree);
				} else {
					$erreur = true;
				}

			} else {
				$erreur = true;
			}

			return $erreur;
		}

		public function reviseStatut($finRevise, $projet){
			//On a la fin revisée
			//On a l'ID du projet
			//Check si l'un des deux boutons a été pesé (meme entrée dans le POST)
			//Si oui, get la valeur, sinon:
			//	Aller chercher la fin planifiée
			//	Comparer la fin planifiée et fin revisée
			//		si planifiée < revisée, en retard, sinon en date
			//		On se fou du début pour l'instant

			$statut = 0;

			//Post contains str, we need int
			if(isset($_POST["statut_radio"])){
				$statut = intval($_POST["statut_radio"]);
			} else {
				$finPlanif = ProjetsDAO::getFinPlanif($projet);

				if($finPlanif < $finRevise){
					$statut = 2;
				} else {
					$statut = 1;
				}
			}

			return $statut;
		}

		public function ajouterProjet(){
			$erreur = false;

			//Ayoye
			if(isset($_POST["categorie_select"]) && isset($_POST["budget"]) && isset($_POST["debut_planif"])
				&& isset($_POST["fin_planif"]) && isset($_POST["conseil_planif"]) && isset($_POST["description"])){

				if(isset($_POST["annee_projet"])){
					$annee = $_POST["annee_projet"];
				} else {
					$annee = date("Y");
				}

				//Un mod n'a pas besoin de rentrer un gestion_id, c'est storé dans $_Session deja
				if(isset($_SESSION["gestion_id"])){
					$gestionID = $_SESSION["gestion_id"];
				} else if(isset($_POST["gestion_select"])){
					$gestionID = $_POST["gestion_select"];
				} else {
					$gestionID = NULL;
				}

				if($gestionID){
					$categorie = $_POST["categorie_select"];
					$budget = $_POST["budget"];
					$debutPlanif = $this->creerDate($annee, $this->getSelectMoisNum($_POST["debut_planif"]));
					$finPlanif = $this->creerDate($annee, $this->getSelectMoisNum($_POST["fin_planif"]));
					$conseil = $this->creerDate($annee, $this->getSelectMoisNum($_POST["conseil_planif"]));
					$description = $_POST["description"];

					$erreur = projetsDAO::ajouterProjet($categorie, $gestionID, $budget, $conseil, $debutPlanif, $finPlanif, $description);
				}
			} else {
				$erreur = true;
			}

			return $erreur;
		}

		public function changeMdp(){
			//Behaviour change
			//If admin, change others passwords
			//if not just yours
			$erreur = false;

			if(isset($_POST["mdp"]) && isset($_POST["confirmation"])){
				if($_POST["mdp"] === $_POST["confirmation"]){
					if(isset($_POST["usager_select"])){
						$usager = $_POST["usager_select"];
					} else {
						$usager = $_SESSION["user"];
					}

					$hash = password_hash($_POST["mdp"], PASSWORD_DEFAULT);
					$erreur = UsagersDAO::changeMdp($usager, $hash);
				} else{
					$erreur = true;
				}
			} else{
				$erreur = true;
			}

			return $erreur;
		}
	}
