<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/SuivisDAO.php");

	class MoisAjaxAction extends CommunAction {
		public $mois;

		public function __construct($annee){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);
			$this->mois = SuivisDAO::getMoisParAnnee($annee);
		}

		protected function executeAction(){

		}
	}
