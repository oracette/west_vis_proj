<?php
	require_once("action/CommunAction.php");
	require_once("action/DAO/ProjetsDAO.php");

	class CompareComptesAjaxAction extends CommunAction {
		public $comptes;

		public function __construct($date){
			parent::__construct(CommunAction::$VIS_PUBLIQUE);
			$this->comptes = ProjetsDAO::compareComptes($date);
		}

		protected function executeAction(){

		}
	}
