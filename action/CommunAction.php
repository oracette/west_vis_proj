<?php
	//Don't forget to disable these settings after!!!
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting(-1);

	date_default_timezone_set("America/Toronto");

	session_start();
	require_once("action/Constantes.php");
	require_once("action/DAO/Connection.php");
	require_once("action/password.php");

	abstract class CommunAction{
		public static $VIS_PUBLIQUE = 0;
		public static $VIS_MOD = 1;
		public static $VIS_ADMIN = 2;
		public $erreur;
		public $succes;

		private $visibilitePage;

		public function __construct($visibilitePage){
			$this->visibilitePage = $visibilitePage;
		}

		public function execute(){
			if(!empty($_GET["logout"])){
				session_unset();
				session_destroy();
				session_start();
			}

			if(empty($_SESSION["visibilite"])){
				$_SESSION["visibilite"] = CommunAction::$VIS_PUBLIQUE;
			}

			if($_SESSION["visibilite"] < $this->visibilitePage){
				//Pour l'instant, redirige vers index
				header('Location: index.php');
				exit;
			}

			$this->executeAction();
		}

		protected abstract function executeAction();

		public function isLoggedIn(){
			return $_SESSION["visibilite"] > CommunAction::$VIS_PUBLIQUE;
		}

		public static function formatNum($num){
			return number_format($num,0, '.' , ',');
		}

		public function remplirSelectMois(){
			//Bon c'est pas beau mais fallait le faire.
			$result = array();

			array_push($result, array("noMois" => 1, "mois" => "Janvier"));
			array_push($result, array("noMois" => 2, "mois" => "Février"));
			array_push($result, array("noMois" => 3, "mois" => "Mars"));
			array_push($result, array("noMois" => 4, "mois" => "Avril"));
			array_push($result, array("noMois" => 5, "mois" => "Mai"));
			array_push($result, array("noMois" => 6, "mois" => "Juin"));
			array_push($result, array("noMois" => 7, "mois" => "Juillet"));
			array_push($result, array("noMois" => 8, "mois" => "Août"));
			array_push($result, array("noMois" => 9, "mois" => "Septembre"));
			array_push($result, array("noMois" => 10, "mois" => "Octobre"));
			array_push($result, array("noMois" => 11, "mois" => "Novembre"));
			array_push($result, array("noMois" => 12, "mois" => "Décembre"));

			return $result;
		}

		/*Need to check if these are still being used*/
		public function getSelectMoisNum($selectResult){
			return (int)(explode(SPLIT_CHAR, $selectResult)[0]);
		}

		public function getSelectMoisNom($selectResult){
			return (explode(SPLIT_CHAR, $selectResult)[1]);
		}

		public function creerDate($annee, $numMois){
			return $annee . "-" . $numMois . "-00";
		}
	}
