$(function(){
	$().onbeforeunload = clearURL(location.href)

	//Depending on project dropdown selection, get its info & show it somwehere on the page
	$("#projet_select").change(function(){
		var projet = this.value

		$.when(
			$.getJSON({
				url:"projet.php?projet=" + projet,
				success: function(data){
					//Attributs: budget, conseil, debut, fin
					$("#budget_projet").text(data.budget + " $")
					$("#debut_projet").text(data.debut)
					$("#fin_projet").text(data.fin)
					$("#conseil_projet").text(data.conseil)
				}
			}),

			//(new Date).getMonth pour aller chercher le mois courant...javascript commence les mois à l'index 0, c'est donc le mois précédent
			//Pcq en php on commence a l'index 1
			$.getJSON({
				url:"suivi.php?projet=" + projet + "&mois=" + (new Date).getMonth(),
				success: function(data){
					//Attributs: cout, statut, debut, fin, conseil
					$("#cout_suivi").text(data.cout + " $")
					$("#statut_suivi").text(data.statut)
					$("#debut_suivi").text(data.debut)
					$("#fin_suivi").text(data.fin)
					$("#conseil_suivi").text(data.conseil)

					$("#statut_suivi").parent().css("background-color", data.couleur)

					//hmmm not exactly how I'd like it to be done
					if(data.statut === "En retard"){
						$("#statut_suivi").parent().css("color","#333")
					} else {
						$("#statut_suivi").parent().css("color","white")
					}
				}
			})

		).then(function(){
			$("#donnees_projet").fadeIn(450, function(){
				$("#suivi_precedent").fadeIn(450)
			})

		})
	})

	//On tab select, set donnees_projet display:none, reset forms
	$("#tab_menu").on( 'shown.bs.tab', 'a[data-toggle="tab"]',function(){
		$("#suivi_precedent").fadeOut(350, function(){
			$("#donnees_projet").fadeOut(350)
		})

		$("form").each(function(){
			this.reset()
		})
	})
})

function clearURL(url){
	url = url.split( '?' )[0]
	window.history.pushState("", "", url);
}

//Prend une date en forma aaaa-mm-jj et la mets en aaaa-mm
function transformDate(date){
	var result = date.slice(0,7)

	if(result === "0000-00"){
		result = "aucune date planifiée"
	}

	return result
}
