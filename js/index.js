$(function() {
	var options= {
		strokeWidth: 20,
		duration: 1200,
		easing: 'easeInOut',
		trailColor:"#b3b3b3",
		trailWidth: 20
	}

	//Données JSON suivent la structure suivante:
	// Statuts --> données de statut --> couches --> couche --> données de couche
	var total = 0

	if($("#nbProjets").length){
		total = parseInt($("#nbProjets").text())
	}

	if( $("#wrap_all_stats").length ){
		$.getJSON('donneesDonut.json').done(function(json){
			json.forEach(function(stat, key){
				for(couche in stat.couches){
					creerCoucheDonut(stat.id, stat.couches[couche], total, stat.couleur, options)
				}
			})
		})
	}

	if( $("#table_suivis").length){
		var table = $("#table_suivis").DataTable({
			"aaSorting": [],
			searching: false,
			paginate: false,
			columnDefs: [
				//Peut pas désactiver l'ordonnance pour tous et ensuite activer pour les 4 seuls où on en a besoin...
				//Faut le faire de la mauvaise manière hein!
				{"bSortable": false, "aTargets":[0,5,6,7,8,9,10,11,12,13,14,15,16]}
			],
			info: false
		})

		//Colonnes d'indexes (pas nécessairement indexes de DB)
		table.on( 'order.dt search.dt', function () {
			table.column(0, {}).nodes().each( function (cell, i) {
				cell.innerHTML = i+1;
			} );
		} ).draw();

		fadeInTout()
	}

	//Get les mois par AJAX, pour seulement avoir les mois où l'on a des données
	$("#annee_select").change(function(){
		$.getJSON({
			url:"mois.php?annee=" + this.value,
			success: function(data){
				$.each(data, function(index, mois){
					//attributs: mois, nomMois
					$('#mois_select').append($('<option>', {
					    value: mois,
					    text: mois
					}));
				})
			}
		})

		$("#mois_select").prop('disabled', false);
		$("#mois_select").prop('required', true);
	})

	//month is selected --> can select gestionnaire
	$("#mois_select").change(function(){
		$("#gestion_select").prop('disabled', false);
		$("#gestion_select").prop('required', true);

		//Get gestions of projets / suivis where counts don't match and colour the backgorund of options
		var date = $("#annee_select").val() + "-" + (this.value).charAt(0) + "-01"

		$("#gestion_select").children().each(function(){
			/*Need to reset colors...*/
			if($(this).is(":enabled")){
				$(this).css("background-color","white")
				$(this).css("color","black")
			}
		})

		$.getJSON({
			url:"comparecomptes.php?date=" + date,
			success: function(data){
				$.each(data, function(index, id){
					var lol = false
					$("#gestion_select").children().each(function(){
						/*$(this).css("background-color","white")
						$(this).css("color","black")*/

						if((this.value).charAt(0) == id){
							$(this).css("background-color","#ffa64d")
							$(this).css("color","white")
							lol = true
						}
					})
				})
			}
		})

	})

	/*$("#gestion_select").change(function(){
		var date = $("#annee_select").val() + "-" + ($("#mois_select").val()).charAt(0)
		console.log(date)
	})*/
})

function creerCoucheDonut(statID, couche, total, couleur, opts){
	var compte = couche.compte
	var id = "#" + "stat_" + statID + "_"  + couche.id
	var classe = "donut_" + couche.id

	$(id).addClass(classe)

	bar = new ProgressBar.SemiCircle(id, opts)
	bar.path.setAttribute("stroke", couleur)

	bar.animate(compte / total)
}

function fadeInTout(){
	var fadeTime = 450	//en milisecondes

	$("#selections").fadeIn(fadeTime, function(){
		$(".stat_wrapper").fadeIn(fadeTime, function(){
			//Data table will add some elements inside the wrapper for some options, need to fade those in to.
			$("#table_suivis_wrapper").fadeIn(fadeTime)
			$("#table_suivis").fadeIn(fadeTime)
		})
	})
}
